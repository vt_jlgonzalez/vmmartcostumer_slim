<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("VM Mart Costumer Portal'/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);

});

// User routes ---------------------------------------------->


/*
 * User login
 * Method: Post
 * @param string $user the username
 * @param string $pass the password
 */

$app->post('/login', 'UserController:login');
$app->get('/api/login', 'UserController:loginApi'); //method is get just for demo purposes



