<?php

namespace App\Controllers;
use Slim\Http\Request;
use Slim\Http\Response;

class UserController{

    private $container;

    public function __construct($container) {
        $this->renderer = $container['renderer'];
        $this->logger = $container['logger'];
    }

    public function login (Request $request, Response $response, array $args){

        $parameters = $request->getParams();

        //Do something to check if user and password matches

        // Sample log message
        $this->logger->info("VM Mart Costumer Portal'/' ". $parameters['user']."Logged In!!");

        // Render index view
        return $this->renderer->render($response, 'dashboard.phtml', $parameters);


    }

    public function loginApi (Request $request, Response $response, array $args){

        $parameters = $request->getParams();

        //Do something to check if user and password matches

        $success['title'] = 'success';
        $success['message'] = 'Logged In';
        $success['status'] = 200;
        $success['session'] = 'ashjdkah1ip32uiurh23uh423';

        return $response->withJson($success, 200, JSON_UNESCAPED_UNICODE);


    }
}
?>